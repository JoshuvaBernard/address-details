let cus = require("./country+university+site.json");

for (let i = 0; i < cus.length; i++) {
  delete cus[i].web;
  delete cus[i].country;
}

// var thisIsObject = {
//   Cow: "Moo",
//   Cat: "Meow",
//   Dog: "Bark",
// };
// delete thisIsObject.Cow;
// console.log(thisIsObject);

var fileSystem = require("fs");
// const client = [
//   {
//     Name: "Mini Corp.",
//     Order_count: 83,
//     Address: "Little Havana",
//   },
// ];
const data = JSON.stringify(cus);

fileSystem.writeFile("./universities.json", data, (err) => {
  if (err) {
    console.log("Error writing file", err);
  } else {
    console.log("JSON data is written to the file successfully");
  }
});

const josh = '{"result":true, "count":42}';
const obj = JSON.parse(josh);
