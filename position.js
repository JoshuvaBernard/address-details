// key e84d0c7726d2a1f893f3f9bcd9d4a617

// const axios = require("axios");
// console.log("w1");
// const params = {
//   access_key: "e84d0c7726d2a1f893f3f9bcd9d4a617",
//   query: "Rio de Janeiro",
// };

// axios
//   .get("https://api.positionstack.com/v1/forward", { params })
//   .then((response) => {
//     console.log(response.data);
//   })
//   .catch((error) => {
//     console.log("error");
//   });

// const axios = require('axios');

// axios.get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY')
//   .then(response => {
//     console.log(response.data.url);
//     console.log(response.data.explanation);
//   })
//   .catch(error => {
//     console.log(error);
//   });

const superagent = require("superagent");

superagent
  .get("https://api.positionstack.com/v1/forward")
  .query({ api_key: "e84d0c7726d2a1f893f3f9bcd9d4a617", date: "2017-08-02" })
  .end((err, res) => {
    if (err) {
      return console.log("err");
    }
    console.log(res.body.url);
    console.log(res.body.explanation);
  });
