const data = require("./countries+states+cities.json");
const universities = require("./universities.json");

// let address = "Cochin University, cochin, kerala, india";

// splits the address with respect to comma(,), removes whites spaces and uppercase first letter
const splitter = function (address) {
  // splits the address with respect to comma(,)
  let temp;
  // to lower case
  temp = address.toLowerCase();
  let splittedAddress = [];
  // splits wrt to comma
  splittedAddress = temp.split(",");
  //removes whites spaces and uppercase first letter
  for (let j = 0; j < splittedAddress.length; j++) {
    splittedAddress[j] = splittedAddress[j].trim();
    splittedAddress[j] =
      splittedAddress[j].charAt(0).toUpperCase() + splittedAddress[j].slice(1);
  }
  return splittedAddress;
};
// searches the address with a database
const search = function (address, database) {
  let name;
  let index;
  for (let i = 0; i < address.length; i++) {
    for (let j = 0; j < database.length; j++) {
      if (address[i] == database[j].name) {
        name = database[j].name;
        index = j;
        return {
          name: name,
          index: index,
        };
      }
    }
  }
  return {
    name: name,
    index: index,
  };
};
// removes that element from array
const remove = function (splittedAddress, Name) {
  const indexOfAddress = splittedAddress.indexOf(Name);
  if (indexOfAddress > -1) {
    splittedAddress.splice(indexOfAddress, 1); // 2nd parameter means remove one item only
  }
  return splittedAddress;
};
// finds the city state and country and return a object
const cityStateCountry = function (address, id) {
  let splittedAddress = splitter(address);

  // find country
  let country = search(splittedAddress, data);
  let countryName = country.name;
  let countryIndex = country.index;
  splittedAddress = remove(splittedAddress, countryName);

  let state, stateName, stateIndex;
  let city, cityName, cityIndex;
  let addressObj = {
    id: "",
    fullAddress: "",
    department: "",
    university: "",
    city: "",
    state: "",
    country: "",
  };

  // find state
  if (countryName) {
    state = search(splittedAddress, data[countryIndex].states);
    stateName = state.name;
    stateIndex = state.index;
    splittedAddress = remove(splittedAddress, stateName);
  }
  // if country is not given
  else {
    for (let i = 0; i < data.length; i++) {
      state = search(splittedAddress, data[i].states);
      if (state.name) {
        stateName = state.name;
        stateIndex = state.index;
        splittedAddress = remove(splittedAddress, stateName);
        break;
      }
    }
  }

  // find city
  // if state is given
  if (stateName) {
    city = search(
      splittedAddress,
      data[countryIndex].states[stateIndex].cities
    );
    cityName = city.name;
    cityIndex = city.index;
    splittedAddress = remove(splittedAddress, cityName);
  }
  // only country is given
  else {
    if (countryName) {
      for (let i = 0; i < data[countryIndex].states.length; i++) {
        city = search(splittedAddress, data[countryIndex].states[i].cities);
        if (city.name) {
          cityName = city.name;
          cityIndex = city.index;
          splittedAddress = remove(splittedAddress, cityName);
          break;
        }
      }
    }
    // both country and state is not given
    else {
      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < data[i].states.length; j++) {
          city = search(splittedAddress, data[i].states[j].cities);
          if (city.name) {
            cityName = city.name;
            cityIndex = city.index;
            splittedAddress = remove(splittedAddress, cityName);
            break;
          }
        }
      }
    }
  }
  addressObj.fullAddress = address;
  addressObj.city = cityName;
  addressObj.state = stateName;
  addressObj.country = countryName;
  addressObj.id = id;

  return {
    addressObj: addressObj,
    remainingAddress: splittedAddress,
  };
};

// let address = "biology department, Cochin University, Cochin ,kerala, india";
let address =
  "biology department, Zimbabwe Open University, Cochin ,kerala, india";
// console.log(cityStateCountry(address));

// let address = [
//   "Peking University health Science Centre, Beijing, beijing, china",
//   "Huadong Hospital Affilated to fudan University, Shanghai, Shanghai, China",
//   "Sichuan Univeristy, Chengdu, Sichuan, China",
//   "Sun Yat-Sen University, Guandzhou, Guangdong, China",
//   "Yangzhou University, Yamgzhou, Jiangsu, China",
//   "Department of Epidemiology and Biostatistics, Peking University Health Science Centre, Beijing, Beijing, China",
//   "National Institute for Communicable Disease Control and Prevention, Beijing, China",
//   "Peking University First Hospital, Beijing, China",
//   "Duke University Medical Center, Durham, North Carolina, United States", /////
//   "Chinese Preventive Medicine Association, Beijing, China",
//   "China National Center for Food Safety Risk Assessment, Beijing, China",
//   "Epidemiology, Peking University Health Science Centre, Beijing, Beijing, China",
//   "Psychology Department, University of Haifa, Haifa, Israel",
//   "Cochin University, cochin, kerala, india",
// ];
// for (let i = 0; i < address.length; i++) {
//   console.log(cityStateCountry(address[i], i));
// }

let deptUni = cityStateCountry(address).remainingAddress;
console.log(deptUni);
console.log(search(deptUni, universities));
