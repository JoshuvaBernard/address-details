#  forward
import json

key = 'e84d0c7726d2a1f893f3f9bcd9d4a617'
import http.client, urllib.parse

conn = http.client.HTTPConnection('api.positionstack.com')

params = urllib.parse.urlencode({
    'access_key': key,
    'query': 'University of Haifa, Haifa, Israel',
    'region': 'Israel',
    'limit': 1,
    })

#     'query': 'Copacabana',
#     'region': 'Rio de Janeiro',

# 'query': 'University of Haifa, Haifa, Israel',
# 'region': 'israel',
conn.request('GET', '/v1/forward?{}'.format(params))

res = conn.getresponse()
data = res.read()
print(data.decode('utf-8'))

# data1 = data.decode('utf-8')
# data2 = json.load((data1))
# print(data2["data"])


# backward

# conn = http.client.HTTPConnection('api.positionstack.com')
#
# params = urllib.parse.urlencode({
#     'access_key': 'YOUR_ACCESS_KEY',
#     'query': '51.507822,-0.076702',
#     })
#
# conn.request('GET', '/v1/reverse?{}'.format(params))
#
# res = conn.getresponse()
# data = res.read()
#
# print(data.decode('utf-8'))