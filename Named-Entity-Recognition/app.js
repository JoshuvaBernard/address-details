console.log("test0");
const spacyNLP = require("spacy-nlp");
// default port 6466
// start the server with the python client that exposes spacyIO (or use an existing socketIO server at IOPORT)
var serverPromise = spacyNLP.server({ port: process.env.IOPORT });
// Loading spacy may take up to 15s

const text =
  "The first phase of the Cold War began in the first two years after the end of the Second World War in 1945. The USSR consolidated its control over the states of the Eastern Bloc, while the United States began a strategy of global containment to challenge Soviet power, extending military and financial aid to the countries of Western Europe (for example, supporting the anti-communist side in the Greek Civil War) and creating the NATO alliance.";

const options = ["word"];

// const result = await nlp.parse_nouns(text, options);

// async function josh() {
//   const result = await spacyNLP.parse_named_entities(text, options);
//   console.log(result);
//   return result;
// }

// josh();
// let promise = new Promise(async function (resolve, reject) {
//   const result = await spacyNLP.parse_named_entities(text, options);
//   // console.log(result);
//   resolve(result);
//   reject(new Error("Will this be ignored?"));
// });
// console.log(promise);
