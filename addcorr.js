const axios = require("axios");

const options = {
  method: "GET",
  url: "https://yurisw-address-correction-and-geocoding.p.rapidapi.com/address",
  params: {
    AddressLine1: "506 Fourth Avenue Unit 1",
    AddressLine2: "Asbury Prk, NJ",
  },
  headers: {
    "X-RapidAPI-Key": " ",
    "X-RapidAPI-Host": "yurisw-address-correction-and-geocoding.p.rapidapi.com",
  },
};

axios
  .request(options)
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.error("error");
  });
