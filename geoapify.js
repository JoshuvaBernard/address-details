// cd10723d44d348099c06cf5e696f1c87

// function geocodeAddress() {
//   const address = "Psychology Department, University of Haifa, Haifa, Israel";
//   const myAPIKey = cd10723d44d348099c06cf5e696f1c87;
//   if (!address || address.length < 3) {
//     console.log(
//       "The address string is too short. Enter at least three symbols"
//     );
//     return;
//   }
//   const geocodingUrl = `https://api.geoapify.com/v1/geocode/search?text=${encodeURIComponent(
//     address
//   )}&apiKey=${myAPIKey}`;
//   // call Geocoding API - https://www.geoapify.com/geocoding-api/
//   fetch(geocodingUrl)
//     .then((result) => result.json())
//     .then((featureCollection) => {
//       console.log(featureCollection);
//     });
// }
var fetch = require("node-fetch");
var requestOptions = {
  method: "GET",
};
fetch(
  "https://api.geoapify.com/v1/geocode/search?text=38%20Upper%20Montagu%20Street%2C%20Westminster%20W1H%201LJ%2C%20United%20Kingdom&apiKey=cd10723d44d348099c06cf5e696f1c87",
  requestOptions
)
  .then((response) => response.json())
  .then((result) => console.log(result))
  .catch((error) => console.log("error", error));
